package api.imdb.APIIMDb.repository;

import api.imdb.APIIMDb.models.Actor;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface ActorsRepository  extends JpaRepository<Actor,Integer> {
    List<Actor> findByNameIn(List<String> name);
}
