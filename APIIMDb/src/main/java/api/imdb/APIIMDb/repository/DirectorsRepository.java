package api.imdb.APIIMDb.repository;

import api.imdb.APIIMDb.models.Director;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface DirectorsRepository  extends JpaRepository<Director,Integer> {
    List<Director> findByNameIn(List<String> types);
}
