package api.imdb.APIIMDb.repository;


import api.imdb.APIIMDb.models.ParentalGuidance;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ParentalGuidanceRepository extends JpaRepository<ParentalGuidance,Integer> {
    ParentalGuidance findOneByPg(String pg);
}
