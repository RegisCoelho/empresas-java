package api.imdb.APIIMDb.repository;

import api.imdb.APIIMDb.models.MovieRatings;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface MovieRatingsRepository extends JpaRepository<MovieRatings,Integer> {
    MovieRatings findByUserIdAndMovieId(Integer userId, Integer movieId);
}
