package api.imdb.APIIMDb.models;

import javax.persistence.*;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import java.util.Objects;

@Entity
@Table(name = "pg")
public class ParentalGuidance {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private Integer id;

    @NotNull(message = "Campo pg não pode ser nulo")
    @NotEmpty(message = "Campo pg não pode ser vazio")
    String pg;

    public ParentalGuidance(){}

    public ParentalGuidance(@NotNull(message = "Campo pg não pode ser nulo") @NotEmpty(message = "Campo pg não pode ser vazio") String pg) {
        this.pg = pg;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getPg() {
        return pg;
    }

    public void setPg(String pg) {
        this.pg = pg;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        ParentalGuidance that = (ParentalGuidance) o;
        return Objects.equals(id, that.id) && Objects.equals(pg, that.pg);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, pg);
    }
}
