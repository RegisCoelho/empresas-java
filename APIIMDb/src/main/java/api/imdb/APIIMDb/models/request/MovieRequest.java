package api.imdb.APIIMDb.models.request;

import api.imdb.APIIMDb.models.*;
import com.google.common.base.Objects;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import java.util.ArrayList;
import java.util.List;

public class MovieRequest {

    @NotNull(message = "Campo title não pode ser nulo")
    @NotEmpty(message = "Campo title não pode ser vazio")
    private String title;
    @NotNull(message = "Campo description não pode ser nulo")
    @NotEmpty(message = "Campo description não pode ser vazio")
    private String description;
    @NotNull(message = "Campo pg não pode ser nulo")
    @NotEmpty(message = "Campo pg não pode ser vazio")
    private String pg;

    private Integer vote;
    private List<String> types;
    private List<String> actors;
    private List<String> directors;


    public MovieRequest(){}

    public MovieRequest(
            @NotNull(message = "Campo title não pode ser nulo")
            @NotEmpty(message = "Campo title não pode ser vazio")
                    String title,
            @NotNull(message = "Campo description não pode ser nulo")
            @NotEmpty(message = "Campo decription não pode ser vazio")
                    String description,
            @NotNull(message = "Campo pg não pode ser nulo")
            @NotEmpty(message = "Campo pg não pode ser vazio")
                    String pg,
            Integer vote,
            List<String> types, List<String> actors, List<String> directors
    ) {
        this.title = title;
        this.description = description;
        this.directors = directors;
        this.actors = actors;
        this.vote = vote;
        this.pg = pg;
        this.types = types;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Integer getVote() {
        return vote;
    }

    public void setVote(Integer vote) {
        this.vote = vote;
    }

    public List<String> getActors() {
        return actors;
    }

    public void setActors(List<String> actors) {
        this.actors = actors;
    }

    public List<String> getDirectors() {
        return directors;
    }

    public void setDirectors(List<String> directors) {
        this.directors = directors;
    }

    public String getPg() {
        return pg;
    }

    public void setPg(String pg) {
        this.pg = pg;
    }

    public List<String> getTypes() {
        return types;
    }

    public void setTypes(List<String> types) {
        this.types = types;
    }

    public static Movie toMovie(MovieRequest movieRequest){
        ParentalGuidance g = new ParentalGuidance(movieRequest.getPg());

        List<Types> ts = new ArrayList<>();
        List<String> types = movieRequest.getTypes();

        types.forEach(t -> {ts.add(new Types(t));});

        List<Actor> at = new ArrayList<>();
        List<String> actors = movieRequest.getActors();

        actors.forEach(a -> {at.add(new Actor(a));});

        List<Director> dt = new ArrayList<>();
        List<String> director = movieRequest.getDirectors();

        director.forEach(d -> {dt.add(new Director(d));});

        Movie movie = new Movie();

        movie.setTitle(movieRequest.getTitle());
        movie.setDescription(movieRequest.getDescription());
        movie.setDirectors(dt);
        movie.setActors(at);
        movie.setPg(g);
        movie.setTypes(ts);

        return movie;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        MovieRequest that = (MovieRequest) o;
        return Objects.equal(title, that.title) && Objects.equal(description, that.description) && Objects.equal(directors, that.directors) && Objects.equal(actors, that.actors);
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(title, description, directors, actors);
    }
}
