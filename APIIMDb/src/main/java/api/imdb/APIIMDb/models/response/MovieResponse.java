package api.imdb.APIIMDb.models.response;

import api.imdb.APIIMDb.models.Movie;
import api.imdb.APIIMDb.models.MovieRatings;
import com.google.common.base.Objects;

import java.util.List;

public class MovieResponse {

    Double average_votes;
    Movie movie;

    public MovieResponse(){}

    public MovieResponse(Movie movie, Double average_votes) {
        this.movie = movie;
        this.average_votes = average_votes;
    }

    public Movie getMovie() {
        return movie;
    }

    public void setMovie(Movie movie) {
        this.movie = movie;
    }

    public Double getAverage_votes() {
        return average_votes;
    }

    public void setAverage_votes(Double average_votes) {
        this.average_votes = average_votes;
    }

    public static MovieResponse toResponse(Movie movies){
        List<MovieRatings> votes = movies.getVote();
        Double votes_avg = 0.0;

        if(votes.size() > 0) {
            votes_avg = votes.stream().mapToDouble(MovieRatings::getVote).sum() / votes.size();
        }

        return new MovieResponse(movies, votes_avg);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        MovieResponse that = (MovieResponse) o;
        return Objects.equal(movie, that.movie) && Objects.equal(average_votes, that.average_votes);
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(movie, average_votes);
    }

}