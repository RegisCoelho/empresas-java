package api.imdb.APIIMDb.models;

import javax.persistence.*;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import java.util.Objects;

@Entity
@Table(name = "movies_directors")

class MovieDirector{
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private Integer id;

    @NotNull(message = "Campo moviesId não pode ser nulo")
    @NotEmpty(message = "Campo moviesId não pode ser vazio")
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "movie_id" )
    Movie moviesId;

    @NotNull(message = "Campo directorId não pode ser nulo")
    @NotEmpty(message = "Campo directorId não pode ser vazio")
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "director_id" )
    Director directorId;

    public MovieDirector(){}

    public MovieDirector(@NotNull(message = "Campo moviesId não pode ser nulo")
                       @NotEmpty(message = "Campo moviesId não pode ser vazio")
                               Movie moviesId,
                       @NotNull(message = "Campo directorId não pode ser nulo")
                       @NotEmpty(message = "Campo directorId não pode ser vazio")
                               Director directorId) {
        this.moviesId = moviesId;
        this.directorId = directorId;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Movie getMoviesId() {
        return moviesId;
    }

    public void setMoviesId(Movie moviesId) {
        this.moviesId = moviesId;
    }

    public Director getDirectorId() {
        return directorId;
    }

    public void setDirectorId(Director directorId) {
        this.directorId = directorId;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        MovieDirector that = (MovieDirector) o;
        return Objects.equals(id, that.id) && Objects.equals(moviesId, that.moviesId) && Objects.equals(directorId, that.directorId);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, moviesId, directorId);
    }


}
