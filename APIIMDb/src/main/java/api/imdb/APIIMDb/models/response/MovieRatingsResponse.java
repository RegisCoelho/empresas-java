package api.imdb.APIIMDb.models.response;

import api.imdb.APIIMDb.models.Movie;
import com.google.common.base.Objects;

class MovieRatingsResponse{
    Movie movie;
    Integer vote;

    public MovieRatingsResponse(){}

    public MovieRatingsResponse(Movie movie, Integer vote) {
        this.movie = movie;
        this.vote = vote;
    }

    public Movie getMovie() {
        return movie;
    }

    public void setMovie(Movie movie) {
        this.movie = movie;
    }

    public Integer getVote() {
        return vote;
    }

    public void setVote(Integer vote) {
        this.vote = vote;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        MovieRatingsResponse that = (MovieRatingsResponse) o;
        return Objects.equal(movie, that.movie) && Objects.equal(vote, that.vote);
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(movie, vote);
    }
}
