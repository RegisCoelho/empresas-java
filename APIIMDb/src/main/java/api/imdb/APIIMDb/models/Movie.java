package api.imdb.APIIMDb.models;

import javax.persistence.*;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import java.time.LocalDateTime;
import java.util.List;
import java.util.Objects;

@Entity
@Table(name = "movies")
public class Movie {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private Integer id;
    @NotNull(message = "Campo title não pode ser nulo")
    @NotEmpty(message = "Campo title não pode ser vazio")
    private String title;
    @NotNull(message = "Campo description não pode ser nulo")
    @NotEmpty(message = "Campo description não pode ser vazio")
    private String description;

    @OneToMany(fetch = FetchType.LAZY, cascade = CascadeType.ALL)
    @JoinColumn(name = "vote" )
    private List<MovieRatings> vote;
    @OneToOne(fetch = FetchType.LAZY, cascade = CascadeType.ALL)
    @JoinColumn(name = "pg")
    private ParentalGuidance pg;
    @ManyToMany(fetch = FetchType.LAZY, cascade = CascadeType.ALL)
    @JoinColumn(name = "types")
    private List<Types> types;
    @ManyToMany(fetch = FetchType.LAZY, cascade = CascadeType.ALL)
    @JoinColumn(name = "name")
    private List<Actor> actors;
    @ManyToMany(fetch = FetchType.LAZY, cascade = CascadeType.ALL)
    @JoinColumn(name = "name")
    private List<Director> directors;

    private LocalDateTime createdAt;
    private LocalDateTime deletedAt;

    public Movie(){}

    public Movie(@NotNull(message = "Campo title não pode ser nulo")
                 @NotEmpty(message = "Campo title não pode ser vazio")
                         String title,
                 @NotNull(message = "Campo description não pode ser nulo")
                 @NotEmpty(message = "Campo description não pode ser vazio")
                         String description,
                 List<MovieRatings> vote,
                 ParentalGuidance pg,
                 List<Types> movie_type,
                 List<Actor> movie_actor,
                 List<Director> movie_director) {

        this.title = title;
        this.description = description;
        this.directors = movie_director;
        this.actors = movie_actor;
        this.vote = vote;
        this.pg = pg;
        this.types = movie_type;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public List<MovieRatings> getVote() {
        return vote;
    }

    public void setVote(List<MovieRatings> vote) {
        this.vote = vote;
    }

    public ParentalGuidance getPg() {
        return pg;
    }

    public void setPg(ParentalGuidance pg) {
        this.pg = pg;
    }

    public List<Types> getTypes() {
        return types;
    }

    public void setTypes(List<Types> types) {
        this.types = types;
    }

    public List<Actor> getActors() {
        return actors;
    }

    public void setActors(List<Actor> actors) {
        this.actors = actors;
    }

    public List<Director> getDirectors() {
        return directors;
    }

    public void setDirectors(List<Director> directors) {
        this.directors = directors;
    }

    public LocalDateTime getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(LocalDateTime createdAt) {
        this.createdAt = createdAt;
    }

    public LocalDateTime getDeletedAt() {
        return deletedAt;
    }

    public void setDeletedAt(LocalDateTime deletedAt) {
        this.deletedAt = deletedAt;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Movie movie = (Movie) o;
        return Objects.equals(id, movie.id) && Objects.equals(title, movie.title) && Objects.equals(description, movie.description) && Objects.equals(vote, movie.vote) && Objects.equals(pg, movie.pg) && Objects.equals(types, movie.types) && Objects.equals(actors, movie.actors) && Objects.equals(directors, movie.directors) && Objects.equals(createdAt, movie.createdAt) && Objects.equals(deletedAt, movie.deletedAt);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, title, description, vote, pg, types, actors, directors, createdAt, deletedAt);
    }

}
