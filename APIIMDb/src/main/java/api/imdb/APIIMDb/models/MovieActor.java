package api.imdb.APIIMDb.models;

import javax.persistence.*;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import java.util.Objects;

@Entity
@Table(name = "movies_actors")

class MovieActor{
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private Integer id;

    @NotNull(message = "Campo moviesId não pode ser nulo")
    @NotEmpty(message = "Campo moviesId não pode ser vazio")
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "movie_id" )
    Movie moviesId;

    @NotNull(message = "Campo actorId não pode ser nulo")
    @NotEmpty(message = "Campo actorId não pode ser vazio")
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "actor_id" )
    Actor actorId;

    public MovieActor(){}

    public MovieActor(@NotNull(message = "Campo moviesId não pode ser nulo")
                     @NotEmpty(message = "Campo moviesId não pode ser vazio")
                             Movie moviesId,
                     @NotNull(message = "Campo actorId não pode ser nulo")
                     @NotEmpty(message = "Campo actorId não pode ser vazio")
                             Actor actorId) {
        this.moviesId = moviesId;
        this.actorId = actorId;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Movie getMoviesId() {
        return moviesId;
    }

    public void setMoviesId(Movie moviesId) {
        this.moviesId = moviesId;
    }

    public Actor getActorId() {
        return actorId;
    }

    public void setActorId(Actor actorId) {
        this.actorId = actorId;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        MovieActor that = (MovieActor) o;
        return Objects.equals(id, that.id) && Objects.equals(moviesId, that.moviesId) && Objects.equals(actorId, that.actorId);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, moviesId, actorId);
    }
}
