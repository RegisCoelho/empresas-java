package api.imdb.APIIMDb.models;

import javax.persistence.*;
import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;
import java.util.Objects;

@Entity
@Table(name = "movie_rating")
public class MovieRatings {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private Integer id;

    @NotNull(message = "Campo MovieId não pode ser nulo")
    Integer movieId;

    @NotNull(message = "Campo UserId não pode ser nulo")
    Integer userId;

    @NotNull(message = "Campo Vote não pode ser nulo")
    @Max(value = 4, message = "O voto deve ser de 0 a 4")
    @Min(value = 0, message = "O voto deve ser de 0 a 4")
    Integer vote;

    public MovieRatings(){}

    public MovieRatings(@NotNull(message = "Campo movieId não pode ser nulo")
                               Integer movieId,
                       @NotNull(message = "Campo userId não pode ser nulo")
                               Integer userId,
                       @NotNull(message = "Campo vote não pode ser nulo")
                               Integer vote) {
        this.movieId = movieId;
        this.userId = userId;
        this.vote = vote;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getMovieId() {
        return movieId;
    }

    public void setMovieId(Integer movieId) {
        this.movieId = movieId;
    }

    public Integer getUserId() {
        return userId;
    }

    public void setUserId(Integer userId) {
        this.userId = userId;
    }

    public Integer getVote() {
        return vote;
    }

    public void setVote(Integer vote) {
        this.vote = vote;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        MovieRatings that = (MovieRatings) o;
        return Objects.equals(id, that.id) && Objects.equals(movieId, that.movieId) && Objects.equals(userId, that.userId) && Objects.equals(vote, that.vote);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, movieId, userId, vote);
    }
}