package api.imdb.APIIMDb.service;

import api.imdb.APIIMDb.models.ParentalGuidance;

public interface ParentalGuidanceService {
    ParentalGuidance findOneByPG(String pg);
    ParentalGuidance create(ParentalGuidance pg);
}
