package api.imdb.APIIMDb.service.impl;

import api.imdb.APIIMDb.models.Types;
import api.imdb.APIIMDb.repository.TypesRepository;
import api.imdb.APIIMDb.service.TypeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class TypeServiceIMPL implements TypeService {

    @Autowired
    private TypesRepository typesRepository;

    @Override
    public List<Types> create(List<String> types) {

        List<String> typesRequested = new ArrayList<>(types);
        List<Types> byTypesIn = this.typesRepository.findByTypesIn(types);

        byTypesIn.forEach(a -> typesRequested.remove(a.getTypes()));

        List<Types> types_to_save = new ArrayList<>();

        typesRequested.forEach(t -> types_to_save.add(new Types(t)));

        return this.typesRepository.saveAll(types_to_save);
    }

    @Override
    public List<Types> findByTypesIn(List<String> types) {
        return this.typesRepository.findByTypesIn(types);
    }
}
