package api.imdb.APIIMDb.service;

import api.imdb.APIIMDb.models.MovieRatings;
import api.imdb.APIIMDb.models.request.MovieRatingsRequest;

public interface MovieRatingsService {
    MovieRatings create(MovieRatingsRequest request);
}
