package api.imdb.APIIMDb.service;

import api.imdb.APIIMDb.models.User;
import api.imdb.APIIMDb.models.request.UserRequest;
import api.imdb.APIIMDb.models.response.UserResponse;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.security.access.prepost.PreAuthorize;

import java.util.Optional;

public interface UserService {
    User create(UserRequest user);
    Optional<User> findByEmail(String email);
    UserResponse save(User user);
    User editUser(UserRequest user);
    User deleteUser(String email);
    User getUserByPrincipal();

    @PreAuthorize("@authorityChecker.isAllowed(authentication)")
    Page<User> listUsersByPage(Pageable page);
}
