package api.imdb.APIIMDb.service.impl;

import api.imdb.APIIMDb.models.User;
import api.imdb.APIIMDb.models.request.UserRequest;
import api.imdb.APIIMDb.models.response.UserResponse;
import api.imdb.APIIMDb.repository.UserRepository;
import api.imdb.APIIMDb.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.util.Optional;
import javax.persistence.NonUniqueResultException;

@Service
public class UserServiceIMPL implements UserService{
    @Autowired
    private UserRepository userRepository;
    @Autowired
    private PasswordEncoder scriptEncoder;

    @Override
    public User create(UserRequest user) throws NonUniqueResultException{
        Optional<User> usr = Optional.ofNullable(this.userRepository.findOneByEmail(user.getEmail()));

        if(!usr.isPresent()){
            user.setPassword(this.scriptEncoder.encode(user.getPassword()));
            return this.userRepository.save(UserRequest.toUser(user));
        }else{
            throw new NonUniqueResultException("Email ja cadastrado,Insira outro email");
        }
    }

    @Override
    public Optional<User> findByEmail(String email){
        return Optional.ofNullable(this.userRepository.findOneByEmail(email));
    }

    @Override
    public UserResponse save(User user){
        user.setPassword(this.scriptEncoder.encode(user.getPassword()));
        return UserResponse.toResponse(this.userRepository.save(user));
    }

    @Override
    public User editUser(UserRequest userRequest) {
        User user = this.userRepository.findOneByEmail(userRequest.getEmail());

        /*
         * Garantir que usuarios que não são admin, não possam alterar o campo is_admin
         * nem editar outros usuários.
         */
        User userByPrincipal = this.getUserByPrincipal();
        if(userByPrincipal.getIs_admin()) {
            user.setIs_admin(userRequest.getIs_admin());
            user.setName(userRequest.getName());
            user.setPassword(this.scriptEncoder.encode(user.getPassword()));
        }else if(user == userByPrincipal) {
            user.setName(userRequest.getName());
            user.setPassword(this.scriptEncoder.encode(user.getPassword()));
        }else {
            throw new BadCredentialsException("Usuários não administradores não podem alterar outros usuários!");
        }
        return this.userRepository.save(user);
    }

    @Override
    public User deleteUser(String email){
        User user = this.userRepository.findOneByEmail(email);
        user.setDeleted(LocalDateTime.now());
        return this.userRepository.save(user);
    }

    @Override
    public User getUserByPrincipal(){
        Object principal = SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        return this.userRepository.findOneByEmail(((UserDetails)principal).getUsername());
    }

    @Override
    public Page<User> listUsersByPage(Pageable page) {
        return this.userRepository.findAllNotAdmin_orderByName(false, page);
    }
}
