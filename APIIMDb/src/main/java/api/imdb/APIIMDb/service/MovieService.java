package api.imdb.APIIMDb.service;

import api.imdb.APIIMDb.models.Movie;
import api.imdb.APIIMDb.models.MovieRatings;
import api.imdb.APIIMDb.models.request.MovieRequest;
import api.imdb.APIIMDb.models.request.MovieRatingsRequest;
import api.imdb.APIIMDb.models.response.MovieResponse;
import org.springframework.data.domain.Pageable;
import org.springframework.security.access.prepost.PreAuthorize;

import java.util.List;

public interface MovieService {
    @PreAuthorize("@authorityChecker.isAllowed(authentication)")
    Movie create(MovieRequest mvRequest);
    @PreAuthorize("@authorityChecker.isAllowed(authentication)")
    Movie deleteMovie(Integer id);
    @PreAuthorize("@authorityChecker.isAllowed(authentication)")
    Movie editMovie(MovieRequest mvRequest);
    @PreAuthorize("@authorityChecker.isAllowed(authentication)")
    MovieRatings vote(MovieRatingsRequest movieRatingsRequest);

    Movie findByTitle(String title);

    List<MovieResponse> listByFilter_Page(Pageable page, String filter, String value, String Order_By);
    MovieResponse getMovieDetailsById(Integer id);
}
