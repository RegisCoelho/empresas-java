package api.imdb.APIIMDb.service.impl;

import api.imdb.APIIMDb.models.Director;
import api.imdb.APIIMDb.repository.DirectorsRepository;
import api.imdb.APIIMDb.service.DirectorsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class DirectorsServiceIMPL implements DirectorsService {

    @Autowired
    private DirectorsRepository directorsRepository;

    @Override
    public List<Director> create(List<String> directors) {

        List<String> directorsRequested = new ArrayList<>(directors);
        List<Director> byMoviesIn = this.directorsRepository.findByNameIn(directors);

        byMoviesIn.forEach(a -> directorsRequested.remove(a.getName()));

        List<Director> movies_to_save = new ArrayList<>();

        directorsRequested.forEach(t -> movies_to_save.add(new Director(t)));

        return this.directorsRepository.saveAll(movies_to_save);
    }

    @Override
    public List<Director> findByMoviesIn(List<String> movies) {
        return this.directorsRepository.findByNameIn(movies);
    }
}
