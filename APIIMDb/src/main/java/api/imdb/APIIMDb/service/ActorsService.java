package api.imdb.APIIMDb.service;

import api.imdb.APIIMDb.models.Actor;
import org.springframework.security.access.prepost.PreAuthorize;

import java.util.List;

public interface ActorsService {
    @PreAuthorize("@authorityChecker.isAllowed(authentication)")
    List<Actor> create(List<String> type);

    List<Actor> findByMoviesOn(List<String> actors);
}
