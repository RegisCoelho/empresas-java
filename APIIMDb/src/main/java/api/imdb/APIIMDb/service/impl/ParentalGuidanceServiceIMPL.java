package api.imdb.APIIMDb.service.impl;

import api.imdb.APIIMDb.models.ParentalGuidance;
import api.imdb.APIIMDb.repository.ParentalGuidanceRepository;
import api.imdb.APIIMDb.service.ParentalGuidanceService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class ParentalGuidanceServiceIMPL implements ParentalGuidanceService {

    @Autowired
    private ParentalGuidanceRepository pgRepository;

    @Override
    public ParentalGuidance findOneByPG(String pg) {
        return this.pgRepository.findOneByPg(pg);
    }

    @Override
    public ParentalGuidance create(ParentalGuidance pg) {
        return this.pgRepository.save(pg);
    }
}
