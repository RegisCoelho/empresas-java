package api.imdb.APIIMDb.service.impl;


import api.imdb.APIIMDb.models.MovieRatings;
import api.imdb.APIIMDb.models.request.MovieRatingsRequest;
import api.imdb.APIIMDb.repository.MovieRatingsRepository;
import api.imdb.APIIMDb.service.MovieRatingsService;
import api.imdb.APIIMDb.service.MovieService;
import api.imdb.APIIMDb.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class MovieRatingsServiceIMPL implements MovieRatingsService {

    @Autowired
    private MovieRatingsRepository movieRatingRepository;

    @Autowired
    private MovieService movieService;

    @Autowired
    private UserService userService;
    @Override
    public MovieRatings create(MovieRatingsRequest request) {


        Integer movieId = this.movieService.findByTitle(request.getTitle()).getId();
        Integer userId = this.userService.getUserByPrincipal().getId();



        Optional<MovieRatings> mr = Optional.ofNullable(this.movieRatingRepository.findByUserIdAndMovieId(userId, movieId));

        if(!mr.isPresent()){
            MovieRatings movieRating = new MovieRatings(
                    movieId,
                    userId,
                    request.getVote()
            );
            return this.movieRatingRepository.save(movieRating);
        }else{
            mr.get().setVote(request.getVote());
            return this.movieRatingRepository.save(mr.get());
        }

    }
}
