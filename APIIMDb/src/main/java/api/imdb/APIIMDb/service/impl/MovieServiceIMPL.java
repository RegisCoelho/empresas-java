package api.imdb.APIIMDb.service.impl;

import api.imdb.APIIMDb.models.Movie;
import api.imdb.APIIMDb.models.MovieRatings;
import api.imdb.APIIMDb.models.ParentalGuidance;
import api.imdb.APIIMDb.models.Types;
import api.imdb.APIIMDb.models.request.MovieRatingsRequest;
import api.imdb.APIIMDb.models.request.MovieRequest;
import api.imdb.APIIMDb.models.response.MovieResponse;
import api.imdb.APIIMDb.repository.MovieRepository;
import api.imdb.APIIMDb.service.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.time.LocalDateTime;
import java.util.*;
import java.util.stream.Collectors;
import javax.persistence.NonUniqueResultException;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.*;

@Service
public class MovieServiceIMPL implements MovieService {

    @PersistenceContext
    private EntityManager em;

    @Autowired
    private MovieRepository movieRepository;

    @Autowired
    private ParentalGuidanceService pgService;

    @Autowired
    private TypeService typeService;
    @Autowired
    private ActorsService actorsService;
    @Autowired
    private DirectorsService directorsService;

    @Autowired
    private MovieRatingsService movieRatingService;

    @Override
    public Movie create(MovieRequest movieRequest) {

        if(null != movieRequest.getPg()){
            movieRequest.getTypes().forEach(types -> {
                List<String> validPg = Arrays.asList("L", "10", "12", "14", "16", "18");
                if(!validPg.contains(movieRequest.getPg().toUpperCase()) ){
                    throw new IllegalArgumentException("PG inválido, utilize um dos seguintes: L, 10, 12, 14, 16, 18");
                }
            });
        }
        ParentalGuidance valid_pg = Optional.ofNullable(this.pgService.findOneByPG(movieRequest.getPg()))
                .orElseGet(() -> this.pgService.create(new ParentalGuidance(movieRequest.getPg())));

        this.typeService.create(movieRequest.getTypes());
        this.actorsService.create(movieRequest.getActors());
        this.directorsService.create(movieRequest.getDirectors());

        Optional<Movie> movie = Optional.ofNullable(this.movieRepository.findOneByTitle(movieRequest.getTitle()));
        if(!movie.isPresent()){
            Movie to_save = MovieRequest.toMovie(movieRequest);
            to_save.setPg(valid_pg);
            to_save.setTypes(this.typeService.findByTypesIn(movieRequest.getTypes()));
            to_save.setActors(this.actorsService.findByMoviesOn(movieRequest.getActors()));
            to_save.setDirectors(this.directorsService.findByMoviesIn(movieRequest.getDirectors()));
            return this.movieRepository.save(to_save);
        }else{
            throw new NonUniqueResultException("Filme já cadastrado!");
        }
    }

    @Override
    public Movie deleteMovie(Integer id) {
        Movie movie = this.movieRepository.findById(id)
                .orElseThrow(() -> new IllegalArgumentException("Não existe filme com o id informado."));

        if(null != movie.getDeletedAt()){
            throw new IllegalArgumentException("Não existe filme com o id informado.");
        }
        movie.setDeletedAt(LocalDateTime.now());
        return this.movieRepository.save(movie);
    }

    @Override
    public Movie editMovie(MovieRequest movieRequest) {
        ParentalGuidance valid_pg = Optional.ofNullable(this.pgService.findOneByPG(movieRequest.getPg()))
                .orElseGet(() -> this.pgService.create(new ParentalGuidance(movieRequest.getPg())));

        this.typeService.create(movieRequest.getTypes());
        this.actorsService.create(movieRequest.getActors());
        this.directorsService.create(movieRequest.getDirectors());

        Optional<Movie> movie = Optional.ofNullable(this.movieRepository.findOneByTitle(movieRequest.getTitle()));
        if(movie.isPresent()){
            Movie to_save = MovieRequest.toMovie(movieRequest);
            to_save.setPg(valid_pg);
            to_save.setTypes(this.typeService.findByTypesIn(movieRequest.getTypes()));
            return this.movieRepository.save(to_save);
        }else{
            throw new NonUniqueResultException("Não foi possível editar o filme!");
        }
    }

    @Override
    public MovieRatings vote(MovieRatingsRequest movie) {
        try{
            return this.movieRatingService.create(movie);
        }catch (NullPointerException e){
            throw new NullPointerException("Não foi possível adicionar o voto para o filme escolhido!");
        }
    }

    @Override
    public Movie findByTitle(String title) {
        return this.movieRepository.findOneByTitle(title);
    }

    @Override
    public List<MovieResponse> listByFilter_Page(Pageable page, String filter, String value, String orderBy) {

        List<String> valid_filters = Arrays.asList("title", "description", "directors", "actors", "types");
        if(!valid_filters.contains(filter)){
            throw new IllegalArgumentException("Filtro inválido, permitido apenas title (para titulo), description (para descrição)," +
                    " directors (para diretores),  actors (para atores) e types (para genero)");
        }

        CriteriaBuilder builder = this.em.getCriteriaBuilder();
        CriteriaQuery<Movie> query = builder.createQuery(Movie.class);
        Root<Movie> from = query.from(Movie.class);

        Join<Movie, MovieRatings> vote = from.join("vote", JoinType.LEFT);

        List<Predicate> predicates = new ArrayList<>();
        Predicate predicate = builder.and();

        if(filter.equalsIgnoreCase("TYPES")){

            Join<Movie, Types> joinTypes = from.join("types");

            Path<Types> type = joinTypes.get("types");

            predicates.add(builder.and(predicate, builder.equal(type, value)));
        }else {
            predicates.add(builder.and(predicate, builder.like(from.get(filter), "%" + value + "%")));
        }
        predicates.add(builder.isNull(from.get("deletedAt")));
        CriteriaQuery<Movie> order =  query.select(from).where(predicates.toArray(new Predicate[predicates.size()])).orderBy(builder.asc(from.get("title")));


        if(orderBy.equalsIgnoreCase("title")){
            TypedQuery<Movie> typedQuery = this.em.createQuery(order);
            return typedQuery.getResultList().stream()
                    .map(MovieResponse::toResponse)
                    .skip((int) page.getOffset()).limit(page.getPageSize())
                    .collect(Collectors.toList());
        }else{
            TypedQuery<Movie> typedQuery = this.em.createQuery(order);
            return typedQuery.getResultList().stream()
                    .map(MovieResponse::toResponse)
                    .sorted(Comparator.comparing(MovieResponse::getAverage_votes).reversed())
                    .skip((int) page.getOffset()).limit(page.getPageSize())
                    .collect(Collectors.toList());
        }


    }

    @Override
    public MovieResponse getMovieDetailsById(Integer id) {
        Movie movie = Optional.ofNullable(this.movieRepository.findByIdAndDeletedAtIsNull(id))
                .orElseThrow(() -> new IllegalArgumentException("Não existe filme com o id informado."));

        return MovieResponse.toResponse(movie);

    }
}
