package api.imdb.APIIMDb.service;

import api.imdb.APIIMDb.models.Director;
import org.springframework.security.access.prepost.PreAuthorize;

import java.util.List;

public interface DirectorsService {
    @PreAuthorize("@authorityChecker.isAllowed(authentication)")
    List<Director> create(List<String> directors);

    List<Director> findByMoviesIn(List<String> directors);
}
