package api.imdb.APIIMDb.service.impl;


import api.imdb.APIIMDb.models.Actor;
import api.imdb.APIIMDb.repository.ActorsRepository;
import api.imdb.APIIMDb.service.ActorsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class ActorsServiceIMPL implements ActorsService {

    @Autowired
    private ActorsRepository actorsRepository;

    @Override
    public List<Actor> create(List<String> actors) {

        List<String> typesRequested = new ArrayList<>(actors);
        List<Actor> byActorsIn = this.actorsRepository.findByNameIn(actors);

        byActorsIn.forEach(a -> typesRequested.remove(a.getName()));

        List<Actor> types_to_save = new ArrayList<>();

        typesRequested.forEach(t -> types_to_save.add(new Actor(t)));

        return this.actorsRepository.saveAll(types_to_save);
    }

    @Override
    public List<Actor> findByMoviesOn(List<String> actors) {
        return this.actorsRepository.findByNameIn(actors);
    }
}
