package api.imdb.APIIMDb;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ApiimDbApplication {

	public static void main(String[] args) {
		SpringApplication.run(ApiimDbApplication.class, args);
	}

}
